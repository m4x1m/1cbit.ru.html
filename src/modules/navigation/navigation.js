const navigation = (width) => {

    const PARAMS = {
        button     : '.js-navigation-button',
        navigation : $('.js-navigation-mobile'),
    };

    $(document).on('click', PARAMS.button, function(){
        if ( !APP.body.hasClass(APP.navigationClass) ) {
            APP.body.addClass(APP.navigationClass);
        }
        else {
            APP.body.removeClass(APP.navigationClass);
        }
    });
    $(window).on('resize', function(){
        if ( window.deviceWidth > width ) {
            if ( APP.body.hasClass(APP.navigationClass) ) {
                APP.body.removeClass(APP.navigationClass);
            }
        }
    });
    $(document).on('mouseup touchend', function (e) {
        if (
            !PARAMS.navigation.is(e.target) &&
            PARAMS.navigation.has(e.target).length === 0 &&
            !$(PARAMS.button).is(e.target) &&
            $(PARAMS.button).has(e.target).length === 0 &&
            !APP.websiteAside.is(e.target) &&
            APP.websiteAside.has(e.target).length === 0
        ) {
            if ( APP.body.hasClass(APP.navigationClass) ) {
                APP.body.removeClass(APP.navigationClass);
            }
        }
    });
}