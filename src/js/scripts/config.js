const APP = {
	name			: 'by 1cbit',
	body			: $('body'),
	websiteWrapper	: $('.js-website-wrapper'),
	websiteAside	: $('.js-website-aside'),
	desktopXsWidth  : 1199,
	tabletWidth     : 991,
	tabletXsWidth   : 767,
	mobileWidth     : 599,
	mobileXsWidth   : 359,
	navigationClass	: '_nav-opened',
}