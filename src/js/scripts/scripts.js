$(function() {
	console.log(APP.name);
    window.deviceWidth = APP.body.width();

    aboutSlider();
    lazyLoad();
    formValidator();

   navigation(APP.tabletWidth);
});

$(window).on('load resize orientationchange', function(e){
    const deviceWith = APP.body.width();

    if ( e.type === 'load' ) {}
    if ( e.type === 'resize' || e.type === 'orientationchange' ) {
        if ( window.deviceWidth !== deviceWith ) {
            window.deviceWidth = deviceWith;
        }
    }
});