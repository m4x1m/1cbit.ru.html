const lazyLoad = () => {
    document.addEventListener('lazybeforeunveil', function(e){
        const src = e.target.getAttribute('data-image-src');
        if ( src ) {
            e.target.style.backgroundImage = 'url(' + src + ')';
        }
    });
}