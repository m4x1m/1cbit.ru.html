const formValidator = () => {

    const PARAMS = {
        app           : $('.js-form'),
        input         : '.js-input',
        container     : '.js-system-message',
        messageActive : 'system-message--success',
    };

    if ( !PARAMS.app.length ) return false;
    $.validator.setDefaults({
        debug   : false,
        success : 'valid'
    });
    $.validator.addMethod("validateEmail", function(value, element) {
        if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value)) {
            return true;
        } else {
            return false;
        }
    });
    $.validator.addMethod("validatePhone", function (value, element) {
        return value.replace(/\D+/g, '').length > 10;
    });

    PARAMS.app.each(function(){
        $(this).validate({
            errorClass   : "input--error",
            validClass   : "input--success",
            errorElement : "span",
            rules : {
                userName : {
                    required : true
                },
                userEmail : {
                    required      : true,
                    validateEmail : true
                }
            },
            messages : {
                userName  : 'Введите имя',
                userEmail : 'Введите корректный email'
            },

            errorPlacement: function(error, element) {
                const parent = element.parents(PARAMS.input);
                error.appendTo(parent);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents(PARAMS.input).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents(PARAMS.input).removeClass(errorClass).addClass(validClass);
            },
            onfocusout: function(element) {
                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                    this.element(element);
                }
            },
            submitHandler: function(form) {
                const App       = $(form);
                const action    = App.attr('action');
                const input     = App.find(PARAMS.input);
                const msg       = App.parents(PARAMS.container);

                $.ajax({
                    url         : action,
                    type        : 'GET',
                    success : function(){
                        msg.addClass(PARAMS.messageActive);
                        setTimeout(function(){
                            App.trigger('reset');
                            input.removeClass('input--success');
                            msg.removeClass(PARAMS.messageActive);
                        }, 5000);
                    },
                    error   : function(){}
                });
            }
        });
    });
}