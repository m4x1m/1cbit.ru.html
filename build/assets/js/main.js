'use strict';

var APP = {
    name: 'by 1cbit',
    body: $('body'),
    websiteWrapper: $('.js-website-wrapper'),
    websiteAside: $('.js-website-aside'),
    desktopXsWidth: 1199,
    tabletWidth: 991,
    tabletXsWidth: 767,
    mobileWidth: 599,
    mobileXsWidth: 359,
    navigationClass: '_nav-opened'
};
var lazyLoad = function lazyLoad() {
    document.addEventListener('lazybeforeunveil', function (e) {
        var src = e.target.getAttribute('data-image-src');
        if (src) {
            e.target.style.backgroundImage = 'url(' + src + ')';
        }
    });
};
var aboutSlider = function aboutSlider() {

    var PARAMS = {
        slider: $('.js-about-slider')
    };

    if (!PARAMS.slider.length) return false;

    PARAMS.slider.slick({
        slidesToShow: 1,
        verticalSwiping: true,
        vertical: true,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: true,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><svg class="i" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path fill-rule="evenodd" d="M6.794,1.278 L0.732,7.193 C0.469,7.450 0.469,7.836 0.732,8.093 C0.996,8.351 1.391,8.351 1.655,8.093 L7.717,2.179 C8.507,1.407 9.693,1.407 10.484,2.179 L16.545,8.093 C16.809,8.351 17.204,8.351 17.468,8.093 C17.599,7.965 17.599,7.708 17.599,7.579 C17.599,7.450 17.599,7.193 17.599,7.193 L11.538,1.278 C10.879,0.764 10.089,0.378 9.166,0.378 C8.244,0.378 7.453,0.635 6.794,1.278 Z"/></svg></button>',
        nextArrow: '<button type="button" class="slick-next"><svg class="i" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd"  d="M6.794,7.183 L0.732,1.267 C0.469,1.010 0.469,0.624 0.732,0.367 C0.996,0.110 1.391,0.110 1.655,0.367 L7.717,6.284 C8.507,7.055 9.693,7.055 10.484,6.284 L16.545,0.367 C16.809,0.110 17.204,0.110 17.468,0.367 C17.599,0.496 17.599,0.754 17.599,0.881 C17.599,1.010 17.599,1.267 17.599,1.267 L11.538,7.183 C10.879,7.697 10.089,8.083 9.166,8.083 C8.244,8.083 7.453,7.826 6.794,7.183 Z"/></svg></button>',
        dots: false,
        infinite: false,
        touchThreshold: 15,
        responsive: [{
            breakpoint: 1280,
            settings: {
                arrows: false,
                dots: true
            }
        }, {
            breakpoint: 991,
            settings: {
                arrows: false,
                dots: true
            }
        }, {
            breakpoint: 767,
            settings: {
                verticalSwiping: false,
                vertical: false,
                arrows: false,
                dots: true
            }
        }, {
            breakpoint: 599,
            settings: {
                verticalSwiping: false,
                vertical: false,
                arrows: false,
                dots: true
            }
        }]
    });
};
var formValidator = function formValidator() {

    var PARAMS = {
        app: $('.js-form'),
        input: '.js-input',
        container: '.js-system-message',
        messageActive: 'system-message--success'
    };

    if (!PARAMS.app.length) return false;
    $.validator.setDefaults({
        debug: false,
        success: 'valid'
    });
    $.validator.addMethod("validateEmail", function (value, element) {
        if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value)) {
            return true;
        } else {
            return false;
        }
    });
    $.validator.addMethod("validatePhone", function (value, element) {
        return value.replace(/\D+/g, '').length > 10;
    });

    PARAMS.app.each(function () {
        $(this).validate({
            errorClass: "input--error",
            validClass: "input--success",
            errorElement: "span",
            rules: {
                userName: {
                    required: true
                },
                userEmail: {
                    required: true,
                    validateEmail: true
                }
            },
            messages: {
                userName: 'Введите имя',
                userEmail: 'Введите корректный email'
            },

            errorPlacement: function errorPlacement(error, element) {
                var parent = element.parents(PARAMS.input);
                error.appendTo(parent);
            },
            highlight: function highlight(element, errorClass, validClass) {
                $(element).parents(PARAMS.input).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function unhighlight(element, errorClass, validClass) {
                $(element).parents(PARAMS.input).removeClass(errorClass).addClass(validClass);
            },
            onfocusout: function onfocusout(element) {
                if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                    this.element(element);
                }
            },
            submitHandler: function submitHandler(form) {
                var App = $(form);
                var action = App.attr('action');
                var input = App.find(PARAMS.input);
                var msg = App.parents(PARAMS.container);

                $.ajax({
                    url: action,
                    type: 'GET',
                    success: function success() {
                        msg.addClass(PARAMS.messageActive);
                        setTimeout(function () {
                            App.trigger('reset');
                            input.removeClass('input--success');
                            msg.removeClass(PARAMS.messageActive);
                        }, 5000);
                    },
                    error: function error() {}
                });
            }
        });
    });
};

var navigation = function navigation(width) {

    var PARAMS = {

        button: '.js-navigation-button',

        navigation: $('.js-navigation-mobile')

    };

    $(document).on('click', PARAMS.button, function () {

        if (!APP.body.hasClass(APP.navigationClass)) {

            APP.body.addClass(APP.navigationClass);
        } else {

            APP.body.removeClass(APP.navigationClass);
        }
    });

    $(window).on('resize', function () {

        if (window.deviceWidth > width) {

            if (APP.body.hasClass(APP.navigationClass)) {

                APP.body.removeClass(APP.navigationClass);
            }
        }
    });

    $(document).on('mouseup touchend', function (e) {

        if (!PARAMS.navigation.is(e.target) && PARAMS.navigation.has(e.target).length === 0 && !$(PARAMS.button).is(e.target) && $(PARAMS.button).has(e.target).length === 0 && !APP.websiteAside.is(e.target) && APP.websiteAside.has(e.target).length === 0) {

            if (APP.body.hasClass(APP.navigationClass)) {

                APP.body.removeClass(APP.navigationClass);
            }
        }
    });
};

$(function () {

    console.log(APP.name);

    window.deviceWidth = APP.body.width();

    aboutSlider();

    lazyLoad();

    formValidator();

    navigation(APP.tabletWidth);
});

$(window).on('load resize orientationchange', function (e) {

    var deviceWith = APP.body.width();

    if (e.type === 'load') {}

    if (e.type === 'resize' || e.type === 'orientationchange') {

        if (window.deviceWidth !== deviceWith) {

            window.deviceWidth = deviceWith;
        }
    }
});
//# sourceMappingURL=main.js.map
